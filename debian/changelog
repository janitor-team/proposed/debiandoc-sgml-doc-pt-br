debiandoc-sgml-doc-pt-br (1.1.13) unstable; urgency=medium

  * Use SOURCE_DATE_EPOCH.  Closes: #828680
  * Update VCS to new salsa service in compliance to the Debian
    Policy 5.6.26 (version 4.1.5).
  * Bump standard version and compat.
  * Update maintainer to the group address.
  * Use simpler packaging filenames.

 -- Osamu Aoki <osamu@debian.org>  Mon, 16 Jul 2018 18:02:54 +0900

debiandoc-sgml-doc-pt-br (1.1.12) unstable; urgency=medium

  * Use onsgml for the migration from sp to opensp. Closes: #812188

 -- Osamu Aoki <osamu@debian.org>  Wed, 03 Feb 2016 20:48:20 +0900

debiandoc-sgml-doc-pt-br (1.1.11) unstable; urgency=low

  * Changed Maintainer to myself only.
  * Moved VCS and its references.
  * Removed build for PDF to make this package more robust.
  * Fixed dependencies and updated debian/rules.
  * Fixed all lintian bugs:
    * W old-fsf-address-in-copyright-file
    * W postinst-has-useless-call-to-install-docs
    * W prerm-has-useless-call-to-install-docs
    * W ancient-standards-version o 3.6.1 (current is 3.8.3)
    * E build-depends-on-obsolete-package
    * E clean-should-be-satisfied-by-build-depends debhelper
    * W debhelper-but-no-misc-depends debiandoc-sgml-doc-pt-br
    * W package-uses-deprecated-debhelper-compat-version 4

 -- Osamu Aoki <osamu@debian.org>  Sun, 10 Jan 2010 03:39:22 +0900

debiandoc-sgml-doc-pt-br (1.1.10) unstable; urgency=low

  * debian/control:
      * changed 'Maintainer' to 'DebianDoc-SGML Pkgs
      <debiandoc-sgml-pkgs@lists.alioth.debian.org>'
    * added current maintainer, Osamu Aoki and Jens Seidel as 'Uploaders'
  * debian/control: upgraded to Debian Policy 3.6.1 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri, 11 Feb 2005 13:23:32 -0600

debiandoc-sgml-doc-pt-br (1.1.9) unstable; urgency=low

  * debian/control: changed build dependency on 'debhelper' to '(>= 4.1)'
  * debian/control: upgraded to Debian Policy 3.6.0 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 16 Aug 2003 10:06:22 -0500

debiandoc-sgml-doc-pt-br (1.1.8) unstable; urgency=low

  * debian/rules: moved debhelper compatibility level setting to
    'debian/compat' per latest debhelper best practices
  * Removed document sources from binary package and indicated in
    README.Debian how to get the source package

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat,  8 Mar 2003 17:12:36 -0600

debiandoc-sgml-doc-pt-br (1.1.7) unstable; urgency=low

  * debian/rules: upgraded to debhelper v4
  * debian/control: changed build dependency on debhelper accordingly
  * debian/rules: split off 'install' target from 'binary-indep' target
  * debian/control: upgraded to Debian Policy 3.5.8 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Mon, 13 Jan 2003 21:19:44 -0600

debiandoc-sgml-doc-pt-br (1.1.6) unstable; urgency=low

  * New version from Carlos Gomes <capgomes@usa.net>

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 11 May 2002 22:15:24 -0500

debiandoc-sgml-doc-pt-br (1.1.5) unstable; urgency=low

  * debian/control: added missing build dependency on libpaperg and tetex-bin

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun,  4 Nov 2001 16:45:30 -0600

debiandoc-sgml-doc-pt-br (1.1.4) unstable; urgency=low

  * Makefile: moved installation of docs to package build
  * Removed explicit dhelp support since doc-base now takes care of this
  * Makefile: clean-up due to the above
  * debian/control: added missing dependency on sp
  * debian/control: upgraded to Debian Policy 3.5.6

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun,  4 Nov 2001 12:12:59 -0600

debiandoc-sgml-doc-pt-br (1.1.3) unstable; urgency=low

  * debian/control: added tetex-extra to Build-Depends
  * debian/rules: split functionality between Makefile and debian/rules
  * debian/control: upgraded to Debian Policy 3.5.2

 -- Ardo van Rangelrooij <ardo@debian.org>  Thu,  5 Apr 2001 17:56:59 -0500

debiandoc-sgml-doc-pt-br (1.1.2) unstable; urgency=low

  * Upgraded to Debian Policy 3.1.1
  * Added Build-Depends
    (closes: Bug#70309)

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri,  8 Sep 2000 08:44:23 +0200

debiandoc-sgml-doc-pt-br (1.1.1) unstable; urgency=low

  * Removed unnecessary postinst and prerm

 -- Ardo van Rangelrooij <ardo@debian.org>  Mon, 22 Nov 1999 18:11:24 +0100

debiandoc-sgml-doc-pt-br (1.1.0) unstable; urgency=low

  * Initial release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun, 21 Nov 1999 23:15:39 +0100
